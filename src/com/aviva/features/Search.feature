Feature: Google Search Aviva

@positive
Scenario Outline:Search Google positive case
	Given opens Google Search "<url>" in "<browser>"
	When SearchText is entered
	|Aviva|
	And Search Button is clicked
	And capture the test data from the search result
	Then verify the "5" th link in the search result
Examples:
	| url 						 | browser |
	| https://www.google.com.sg/ | CH      |
	| https://www.google.com.sg/ | IE      |
#	| https://www.google.com.sg/ | FF      |

@negative
Scenario Outline:Search Google Negative case
	Given opens Google Search "<url>" in "<browser>"
	When SearchText is entered
	|Aviv|
	And Search Button is clicked
    And capture the test data from the search result
	Then "9" links and fifth link "Log in | Aviva Singapore - Aviva Singapore" are not displayed
Examples:
	| url 						 | browser |
	| https://www.google.com.sg/ | CH      |
    | https://www.google.com.sg/ | IE      |
#	| https://www.google.com.sg/ | FF      |		