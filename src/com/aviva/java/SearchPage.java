package com.aviva.java;

import java.awt.Desktop;
import java.io.File;
import java.net.URL;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SearchPage {

	public static String currentDir;

	public static WebDriver getDriver(String browserType, String url) throws Exception {
		WebDriver driver = null;
		currentDir = System.getProperty("user.dir");
		File file = null;
		switch (browserType) {
		case "CH":
			file = copyDrivers("chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
			ChromeOptions chromeOptions = new ChromeOptions();
			chromeOptions.addArguments("--start-maximized");
			driver = new ChromeDriver(chromeOptions);
			driver.get(url);
			break;
		case "IE":
			DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
			capabilities.setCapability("InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION", true);
			capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			capabilities.setCapability("ignoreZoomSetting", true);
			capabilities.setCapability("ignoreProtectedModeSettings", true);
			capabilities.setCapability("initialBrowserUrl", url);
			file = copyDrivers("IEDriverServer.exe");
			System.setProperty("webdriver.ie.driver", file.getAbsolutePath());
			driver = new InternetExplorerDriver(capabilities);
			driver.manage().window().maximize();
			break;
		case "FF":
			driver = new FirefoxDriver();
			driver.get(url);
			break;
		default:
			file = copyDrivers("chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
			driver = new ChromeDriver();
			driver.get(url);
			break;
		}
		return driver;

	}

	public static WebElement enterSearchText(String data, WebDriver driver) {

		WebDriverWait wait = new WebDriverWait(driver, 10000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("lst-ib")));

		WebElement searchBox = driver.findElement(By.id("lst-ib"));
		searchBox.sendKeys(data);
		searchBox.sendKeys(Keys.ESCAPE);
		return searchBox;

	}

	public static WebElement clickSearchButton(WebDriver driver) {
		WebElement searchButton = driver.findElement(By.name("btnK"));
		searchButton.click();
		return searchButton;

	}
	
	public static List<WebElement> getData(WebDriver driver){
		WebDriverWait wait = new WebDriverWait(driver, 10000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("search")));

		return driver.findElements(By.xpath("//div[contains(@id,'search')]//div[@class = 'rc']/h3/a"));

	}

	public static File copyDrivers(String resourceName) throws Exception {
		URL resource = SearchPage.class.getResource("/com/aviva/drivers/" + resourceName);
		File f = new File("Drivers");
		if (!f.exists()) {
			f.mkdirs();
		}
		File driverFile = new File(currentDir + "/Drivers/" + resourceName);
		if (!driverFile.exists()) {
			FileUtils.copyURLToFile(resource, driverFile);
		}

		return driverFile;

	}

	public static void display_the_reports(){
		try{
		File htmlFile = new File(SearchPage.currentDir + "/Report/HTML/index.html");
		Desktop.getDesktop().browse(htmlFile.toURI());
		}
		catch(Exception e){
			System.out.println("Error in opening the  report file. "+e.getLocalizedMessage());
		}
	}

}
