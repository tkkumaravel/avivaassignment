package com.aviva.java;

import org.junit.AfterClass;
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(/*tags = { "@positive" },*/features = "classpath:com/aviva/features", glue = "classpath:com/aviva/java", 
dryRun = false, monochrome = true,format={"pretty","html:Report/HTML","json:Report/JSON/JsonReport.json","junit:Report/XML/XMLReport.xml"})
public class SearchRunner {
	@AfterClass
	public static void reports(){
		SearchPage.display_the_reports();
	}
}
	

