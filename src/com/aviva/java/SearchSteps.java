package com.aviva.java;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SearchSteps {

	public WebDriver driver;
	public List<WebElement> list;

	@Given("^opens Google Search \"([^\"]*)\" in \"([^\"]*)\"$")
	public void opens_Google_Search_url_in_browser(String url, String browser) {
		try {
			driver = SearchPage.getDriver(browser, url);
			
		} catch (Exception e) {
			driver.quit();
		}
	}

	@When("^SearchText is entered$")
	public void searchtext_is_entered(DataTable data) throws Throwable {
		try {
			List<List<String>> text = data.raw();
			SearchPage.enterSearchText(text.get(0).get(0), driver);
		} catch (Exception e) {
			driver.quit();
		}
	}

	@When("^Search Button is clicked$")
	public void search_Button_is_clicked() {
		try {
			SearchPage.clickSearchButton(driver);
		} catch (Exception e) {
			driver.quit();
		}
	}

	@Then("^verify the \"([^\"]*)\" th link in the search result$")
	public void links_and_fifth_link_is_displayed(Integer count) {
		List<WebElement> objs = null;
		int actualCount = 0;
		String actualURL = null;

		try {
			objs = SearchPage.getData(driver);
			actualCount = objs.size();
			actualURL = objs.get(count-1).getText();
			System.out.println(
					"No Of Links found on the first Search Page:\n ===========> Actual:" + actualCount + "  Expected:" + list.size());
			System.out.println(
					"Fifth Link found on the first Search Page:\n ===========> Actual:" + actualURL + "  Expected:" + list.get(count-1).getText());
			Assert.assertEquals(list.get(count-1).getText().toString(),actualURL.toString());
			Assert.assertTrue(actualCount == list.size());
			driver.quit();

		} catch (Exception e) {
			e.printStackTrace();
			driver.quit();
		}
		
	}

	@Then("^\"([^\"]*)\" links are displayed$")
	public void links_are_displayed(Integer count) throws Throwable {

		List<WebElement> objs = null;
		try {

			WebElement result = driver.findElement(By.id("center_col"));
			objs = result.findElements(By.tagName("a"));
			Assert.assertEquals(objs != null ? objs.size() : 0, count.intValue());

		} catch (NoSuchElementException e) {
			Assert.assertEquals(objs != null ? objs.size() : 0, count.intValue());
			System.out.println("No Links found on the first Search Page");
		}
		driver.quit();
	}
	
	@When("^capture the test data from the search result$")
	public void capture_the_test_data_from_the_search_result() throws Throwable {
		list = SearchPage.getData(driver);
	}

	@Then("^\"([^\"]*)\" links and fifth link \"([^\"]*)\" are not displayed$")
	public void links_and_fifth_link_are_not_displayed(Integer count, String fifthLink) {
		List<WebElement> objs = null;
		int actualCount = 0;
		String actualURL = null;

		try {
			objs = SearchPage.getData(driver);
			actualCount = objs.size();
			actualURL = objs.get(4).getText();
			System.out.println(
					"No Of Links found on the first Search Page:\n ===========> Actual:" + actualCount + "  Expected:" + count);
			System.out.println(
					"Fifth Link found on the first Search Page:\n ===========> Actual:" + actualURL + "  Expected:" + fifthLink);
			Assert.assertNotEquals(fifthLink.toString(),actualURL.toString());
			Assert.assertFalse(actualCount == count);
			driver.quit();

		} catch (Exception e) {
			e.printStackTrace();
			driver.quit();
		}
		

	
	}

}
